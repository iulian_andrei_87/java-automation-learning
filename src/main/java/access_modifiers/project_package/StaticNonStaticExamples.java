package access_modifiers.project_package;

import java.util.Random;

public class StaticNonStaticExamples {

    public int nonStaticVar = 1;
    public static int staticVar = 2;

    public int nonStaticMethod() {
        return new Random().nextInt(100);
    }

    public static int staticMethod() {
        return new Random().nextInt(100);
    }
}
