package access_modifiers.project_package;

import java.util.Random;

public class AccessModifiersExamples {

    private int privateVar = 1;
    protected int protectedVar = 2;
    int defaultVar = 3;
    public int publicVar = 4;

    private int privateMethod() {
        return new Random().nextInt(100);
    }

    protected int protectedMethod() {
        return new Random().nextInt(100);
    }

    int defaultMethod() {
        return new Random().nextInt(100);
    }

    public int publicMethod() {
        return new Random().nextInt(100);
    }

    /**
     * Current location - within its package
     *
     * private      -   within class
     * protected    -   derived classes/same package
     * package      -   within its package
     * public       -   everybody
     */
    public void accessExamples() {
        AccessModifiersExamples accessModifiersExamples = new AccessModifiersExamples();

        int var1 = accessModifiersExamples.publicVar;
        int var2 = accessModifiersExamples.publicMethod();

        int var3 = accessModifiersExamples.defaultVar;
        int var4 = accessModifiersExamples.defaultMethod();

        int var5 = accessModifiersExamples.privateVar;
        int var6 = accessModifiersExamples.privateMethod();

        int var7 = accessModifiersExamples.protectedVar;
        int var8 = accessModifiersExamples.protectedMethod();
    }
}
