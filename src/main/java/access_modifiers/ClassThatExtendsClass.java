package access_modifiers;

import access_modifiers.project_package.AccessModifiersExamples;
import access_modifiers.project_package.StaticNonStaticExamples;

public class ClassThatExtendsClass extends AccessModifiersExamples {
    public void staticNonStaticExamples() {
        int dVar1 = StaticNonStaticExamples.staticMethod();                                     //asa se apeleaza o METODA statica
        int dVar2 = StaticNonStaticExamples.staticVar;                                          //asa se apeleaza o VARIABILA statica

        int iVar1 = new StaticNonStaticExamples().nonStaticMethod();                            //asa se apeleaza o METODA non statica v1

        StaticNonStaticExamples staticNonStaticExamplesObj = new StaticNonStaticExamples();     //asa se apeleaza o METODA non statica v2
        int iVar2 = staticNonStaticExamplesObj.nonStaticMethod();                               //asa se apeleaza o METODA non statica v2
        int iVar3 = staticNonStaticExamplesObj.nonStaticVar;                                    //asa se apeleaza o VARIABILA non statica
    }

    /**
     * Current location - derived classes/outside its package
     *
     * private      -   within class
     * protected    -   derived classes/within its package
     * package      -   within its package
     * public       -   everybody
     */
    public void accessExamples() {
        int var1 = publicVar;
        int var2 = publicMethod();

//        int var3 = defaultVar;
//        int var4 = defaultMethod();

//        int var5 = privateVar;
//        int var6 = privateMethod();

        int var7 = protectedVar;
        int var8 = protectedMethod();
    }
}
