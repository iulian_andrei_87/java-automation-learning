package inheritance;

import java.util.Random;

abstract class AbsClass {
    String className;
    String inAbs;

    public AbsClass(String className) {
        this.className = className;
        inAbs = className + "-" + new Random().nextInt(100);
    }

    abstract String getClsName();

    public String getInAbs() {
        return inAbs;
    }
}
