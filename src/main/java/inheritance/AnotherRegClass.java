package inheritance;

public class AnotherRegClass extends RegClass {

    private String var = "test";

    public AnotherRegClass(String className) {
        super(className);
    }

    @Override
    public String getClsName() {
        return this.className;
    }

    public class Internal {
        public String getVar() {
            return var;
        }
    }
}
