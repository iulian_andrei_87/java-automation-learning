package inheritance;

import objects_examples.RegularClass;

public class Run {
    public static void main(String[] args) {

        AnotherRegClass anotherRegClass = new AnotherRegClass("AnotherRegClass");
        RegClass regClass = new RegClass("RegClass");

        System.out.println(anotherRegClass.getInAbs());

        System.out.println(regClass.getInAbs());

        AnotherRegClass arg = new AnotherRegClass("test");
        AnotherRegClass.Internal var = arg.new AnotherRegClass.Internal();
    }
}
