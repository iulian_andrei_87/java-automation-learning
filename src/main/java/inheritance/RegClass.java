package inheritance;

public class RegClass extends AbsClass {

    public RegClass(String className) {
        super(className);
    }

    @Override
    public String getClsName() {
        return this.className;
    }
}
