package var_learning;

public class ArrayExamples {

    public static void main(String[] args) {


        String[] limitStringArray = new String[7];
        int[] intArray = new int[7];

        limitStringArray[0] = "test1";
        limitStringArray[1] = "test2";
        limitStringArray[2] = "test3";
        limitStringArray[3] = "test4";
        limitStringArray[4] = "test5";
        limitStringArray[5] = "test6";
        limitStringArray[6] = "test5";

        //     System.out.println(limitStringArray.length);
        find2("test6", limitStringArray);
    }


    public static void find(String element, String[] array) {
        boolean flag = false;

        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                flag = true;
                break;
            }
        }

        if (flag) {
            System.out.println("Element exists: " + element);
        } else {
            System.out.println("Elementul:  " + element + " nu exista!");
        }
    }

    public static void find2(String element, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                System.out.println("Element exists: " + element);
                return;
            }
        }

        System.out.println("Elementul:  " + element + " nu exista!");
    }


    public static void count(String element, String[] array) {
        int contor = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                contor++;
            }
        }

        System.out.println(contor);
    }
}
