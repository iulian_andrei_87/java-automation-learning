package var_learning;

import java.util.Scanner;

public class QuizWithMethods {

    private static Scanner scan = new Scanner(System.in);
    private static int correctResponses = 0;

    private static String q1 = "1. Which battle of 1571 marked the end of the Ottoman naval supremacy in the Mediterranean?";
    private static String q2 = "2. How many years did \"The Hundred Years War\" last?";
    private static String q3 = "3. Did Julius Caesar conquer Gaul, imprisoning and ultimately executing their commander Vercingetorix in the process?";
    private static String q4 = "4. What was the life expectancy at birth, in 18th century Prussia?";

    private static String a1 = "The Battle of Lepanto";
    private static int a2 = 116;
    private static boolean a3 = true;
    private static double a4 = 24.7;


    public static void main(String[] args) {
        System.out.println(q1);
        if (getStringResponse().equals(a1)) correctResponses += 1;

        System.out.println(q2);
        if (getIntResponse() == a2) correctResponses += 1;

        System.out.println(q3);
        if (getBooleanResponse() == a3) correctResponses += 1;

        System.out.println(q4);
        if (getDoubleResponse() == a4) correctResponses += 1;

        showLevel(correctResponses, 4);
    }


    private static int getIntResponse() {
        int result = scan.hasNextInt() ? scan.nextInt() : -1;
        resetScanner();

        return result;
    }

    private static String getStringResponse() {
        String result = scan.hasNextLine() ? scan.nextLine() : "";
        resetScanner();

        return result;
    }

    private static double getDoubleResponse() {
        double result = scan.hasNextDouble() ? scan.nextDouble() : -1.0;
        resetScanner();

        return result;
    }

    private static boolean getBooleanResponse() {
        boolean result = scan.hasNextBoolean() && scan.nextBoolean();
        resetScanner();

        return result;
    }

    private static void resetScanner() {
        scan = new Scanner(System.in);
    }

    private static void showLevel(double correctResponses, double totalQuestions) {
        System.out.println();
        System.out.print("Your level is: ");

        double percentage = (correctResponses / totalQuestions) * 100;

        if (percentage == 0) {
            System.out.print("Weak AF - " + Math.round(percentage) + "%");
        } else if (percentage > 0 && percentage <= 25) {
            System.out.print("Lucky Bastard - " + percentage + "%");
        } else if (percentage > 25 && percentage <= 50) {
            System.out.print("(Joe Bowers) Average - " + percentage + "%");
        } else if (percentage > 50 && percentage <= 75) {
            System.out.print("History buff - " + percentage + "%");
        } else {
            System.out.print("History professor - " + percentage + "%");
        }
    }
}
