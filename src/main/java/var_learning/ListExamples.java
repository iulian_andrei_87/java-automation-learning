package var_learning;

import java.util.*;

public class ListExamples {
    public static void main(String[] args) {

        List<String> listString = new ArrayList<>();

        List<String> test = new LinkedList<>();

        List<String> test2 = new Vector<>();

        Map<String, String> test3 = new HashMap<>();

        ArrayList<String> firstList = new ArrayList<>();
        firstList.add("test1");
        firstList.add("test2");
        firstList.add("test3");
        firstList.add("test4");
        firstList.add("test5");
        firstList.add("test6");



        String test123 = "test cu spatiue";

        while (test123.contains(" ")) {
            test123 = test123.replace(" ", "");
        }

        ArrayList<String> secondList = new ArrayList<>();
        secondList.add("this element");
        secondList.add("this second element");


        firstList.addAll(secondList);

//        System.out.println(firstList.toString());
//        System.out.println(firstList.get(2));
//
//        System.out.println("All elements of list: " + firstList.toString());
//        System.out.println("3rd element: " + firstList.get(3));
//        firstList.remove(3);
//        System.out.println("All elements of list: " + firstList.toString());
//        System.out.println("3rd element: " + firstList.get(3));


//        firstList.clear();
//        System.out.println(firstList.toString());

//        System.out.println(firstList.contains("test"));
//
//
//        findElement("test1", firstList);

        replaceTest("test", "example", firstList);
        System.out.println(firstList.toString());
    }


    public void defineList() {
        List<String> stringList;
        String[] stringArray;


        List<Integer> intList;

        Integer[] intArray1;
        int[] intArray2;
    }

    public static void findElement(String element, ArrayList<String> listOfStrings) {

        if (listOfStrings.contains(element)){
            System.out.println("element exists");
        } else {
            System.out.println("doesn't exist");
        }


    }

    public static void replaceTest(String elementToFind, String elementToReplace , List<String>listsOfStrings){

        for (int i = 0; i < listsOfStrings.size(); i++) {
            if (listsOfStrings.get(i).contains(elementToFind)){
                listsOfStrings.remove(i);
                listsOfStrings.add(i, listsOfStrings.get(i).replace(elementToFind,elementToReplace));
            }
        }
    }

    public void forElseExample(List<String>listsOfStrings) {
        for (String elem : listsOfStrings) {
            System.out.println(elem);
        }
    }

}
