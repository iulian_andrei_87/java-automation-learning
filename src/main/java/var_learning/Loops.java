package var_learning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Loops {

    static ArrayList<String> firstList = new ArrayList<>();
    private static HashMap<String, String> persons = new HashMap<>();

    static {
        firstList.add("test1");
        firstList.add("test2");
        firstList.add("test3");
        firstList.add("test4");
        firstList.add("test5");
        firstList.add("test6");

        persons.put("Andrei", "Iulian");
        persons.put("Dascalu", "Mihai");
        persons.put("Zlota", "Silviu");
        persons.put("Di Conza", "Domenico");
        persons.put("key", "value");
    }

    public static void main(String[] args) {
        doWhileLoop();
    }

    /**
     * Works best when a value needs to be evaluated before executing the loop
     */
    public static void whileLoop() {
//        //syntax
//        while (boolean condition) { //stops on false
//            // loop statements
//        }

        //while equivalent of for
        int i = 0;
        while (i < firstList.size()) {
            System.out.println(firstList.get(i));
            i++;
        }

        boolean flag = true;
        int n1 = 0;

        while (flag) {
            n1 += 1;
            System.out.println(n1);

            if (n1 == 10) {
                flag = false;
            }
        }

        String str = "test replace with while";
        System.out.println(str);

        while (str.contains(" ")) {
            str = str.replaceFirst(" ", "_");
        }

        System.out.println(str);
    }

    /**
     * Works best when an action needs to be executed and after a condition needs to be evaluated
     */
    public static void doWhileLoop() {
//        do {
//            // loop statements
//        } while (boolean condition); //stops on false

        String str = "test replace with while";
        System.out.println(str);

        int i = 0;

        do {
            str = str.replaceFirst(" ", "_");
            i++;
            System.out.println(i);
        } while (str.contains(" "));

        System.out.println(str);
    }

    /**
     * Works best on arrays/lists because it generates numbers (int i = 0) that can be used to get values from the array/list
     */
    public static void forLoop() {
//        //syntax
//        for  (initialization condition; testing condition; increment/decrement) {
//            // loop statements
//        }

        for (int i = 0; i < firstList.size(); i++) {
            System.out.println(firstList.get(i));
        }
    }

    /**
     * Works best for maps and sets as they are no numeric keys to iterate through. Also is best suited for when all elements need to be evaluated;
     */
    public static void forEachLoop() {
//        //syntax
//        for (Type name : array/list/set/map) {
//            // loop statements
//        }

        Set<String> keys = persons.keySet();

        for (String localVarKey : keys) {
            System.out.println("Key is: " + localVarKey);
            System.out.println("Values is: " + persons.get(localVarKey));
            System.out.println();
        }
    }
}