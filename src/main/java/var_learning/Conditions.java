package var_learning;

public class Conditions {
    public static void main(String[] args) {
//        printStringEquals("firstString", "secondString");

        //        System.out.println(result);
//        System.out.println(s1 > s2);
//        System.out.println(s1 >= s2);
//        System.out.println(s1 <= s2);
//        System.out.println(s1 < s2);
//        System.out.println(false);
//        System.out.println(true);
//        System.out.println(s1 == s2 && s1 <= s2);
//        System.out.println(s1 == s2 || s1 <= s2);

//        System.out.println(printStringEquals(1200, 1201));


        printSwitch(0);
    }

    public static void printSwitch(int value) {

        switch (value) {
            case 0:
                System.out.println("case 0");
            case 1:
                System.out.println("case 1");
                break;
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
                break;
        }

        System.out.println("past default");
    }

    public void StringSwitch(String str) {

        switch (str) {
            case "test1":
                System.out.println("something");
                break;
            case "test2":
                System.out.println("something");
                break;
            case "test3":
            case "test4":
                System.out.println("something 3 sau 4");
                break;
            case "test5":
                System.out.println("something");
                break;
            case "test6":
                System.out.println("something");
                break;
        }


        if (str.equals("test1")) {
            System.out.println("something");
        } else if (str.equals("test2")) {
            System.out.println("something");
        } else if (str.equals("test3") || str.equals("test4")) {
            System.out.println("something");
        } else if (str.equals("test5")) {
            System.out.println("something");
        } else if (str.equals("test6")) {
            System.out.println("something");
        }
    }


    public static int printStringEquals(int s1, int s2) {

        if (s1 == s2) {
            return 1;
        } else if (s1 > s2) {
            return 2;
        } else {
            return 4;
        }

//        System.out.println("after if");
    }
}
