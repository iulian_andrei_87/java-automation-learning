package var_learning;

import java.util.Scanner;

public class QuizNoMethods {

    public static void main(String[] args) {

        String q1 = "1. Which battle of 1571 marked the end of the Ottoman naval supremacy in the Mediterranean?";
        String q2 = "2. How many years did \"The Hundred Years War\" last?";
        String q3 = "3. Did Julius Caesar conquer Gaul, imprisoning and ultimately executing their commander Vercingetorix in the process?";
        String q4 = "4. What was the life expectancy at birth, in 18th century Prussia?";

        String a1 = "The Battle of Lepanto";
        int a2 = 116;
        boolean a3 = true;
        double a4 = 24.7;

        int correctResponses = 0;

        System.out.println(q1);
        Scanner scan = new Scanner(System.in);




        if (scan.nextLine().equals(a1)) {
            correctResponses += 1;
        }

        System.out.println();
        System.out.println(q2);

        scan.reset();
        if (scan.nextInt() == a2) {
            correctResponses += 1;
        }

        System.out.println();
        System.out.println(q3);

        scan.reset();
        if (scan.nextBoolean() == a3) {
            correctResponses += 1;
        }

        System.out.println();
        System.out.println(q4);

        scan.reset();
        if (scan.nextDouble() == a4) {
            correctResponses += 1;
        }

        System.out.println();
        System.out.print("Your level is: ");

        switch (correctResponses) {
            case 0:
                System.out.print("Week AF (0/4)");
                break;
            case 1:
                System.out.print("Lucky Bastard (1/4)");
                break;
            case 2:
                System.out.print("(Joe Bowers) Average (2/4)");
                break;
            case 3:
                System.out.print("History amateur (3/4)");
                break;
            case 4:
                System.out.print("History buff (4/4)");
                break;
        }
    }
}
