package var_learning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class MapExamples {

    //    Map<String, String> persons = new HashMap<>();
    private static HashMap<String, String> persons = new HashMap<>();
    private static HashMap<Integer, Boolean> secondMap = new HashMap<>();

    private static HashMap<String, ArrayList<String>> MapOfLists = new HashMap<>();
    private static ArrayList<HashMap<String, String>> ListOfMaps = new ArrayList<>();

    static {
        persons.put("Andrei", "Iulian");
        persons.put("Dascalu", "Mihai");
        persons.put("Zlota", "Silviu");
        persons.put("Di Conza", "Domenico");
        persons.put("key", "value");


        secondMap.put(0, true);
        secondMap.put(1, false);
        secondMap.put(2, false);
    }

    public static void iterateMapWithSet() {
        Set<String> keys = persons.keySet();

        for (String localVarKey : keys) {
            System.out.println("Key is: " + localVarKey);
            System.out.println("Values is: " + persons.get(localVarKey));
            System.out.println();
        }
    }

    public static void main(String[] args) {

        getFromMap("Zlota");
        System.out.println(containsKeyInMap("Zlot"));
        System.out.println(containsValueInMap("Iulia"));
    }

    public static void iterateMapWithSet2() {

        for (String localVarKey : persons.keySet()) {
            System.out.println("Key is: " + localVarKey);
            System.out.println("Values is: " + persons.get(localVarKey));
            System.out.println();
        }
    }

    public static void addToMap() {

        persons.put("key", "value");
    }

    public static void getFromMap(String key) {

        String val = persons.get(key);
        System.out.println(val);
    }

    public static boolean containsKeyInMap(String key) {
        return persons.containsKey(key);
    }

    public static boolean containsValueInMap(String value) {
        return persons.containsValue(value);
    }

    public static void removeKey(String key) {
        persons.remove(key);
    }

    public static void replace(String key, String value) {
        persons.replace(key, value);
    }

    public static void clearMap(String key, String value) {
        persons.clear();
    }

    public static int sizeOfMap(String key, String value) {
        return persons.size();
    }
}
