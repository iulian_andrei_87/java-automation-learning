package quiz;

public class ViewQuestions {

    private ModelQuestions questions;
    private String currentQuestion;

    public ViewQuestions(ModelQuestions questions) {
        this.questions = questions;
    }

    public void printQuestion(int questionNumber) {
        this.setCurrentQuestion(this.questions.getQuestion(questionNumber));

        System.out.println(this.currentQuestion);
    }

    public void printQuestionVariants() {
        System.out.println(questions.getOptions(this.currentQuestion));
    }

    private void setCurrentQuestion(String currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    private String getCurrentQuestion() {
        return this.currentQuestion;
    }
}
