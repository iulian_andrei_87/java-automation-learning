package quiz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelQuestions {

    private static Map<String, Map<String, Boolean>> questions = new HashMap<>();

    static {
        String q1 = "1. Which battle of 1571 marked the end of the Ottoman naval supremacy in the Mediterranean?";
        Map<String, Boolean> v1 = new HashMap<>();
        v1.put("The Battle of Lepanto", true);
        v1.put("The Battle of Athens", false);
        v1.put("The Battle of Berlin", false);
        v1.put("The Battle of Bucharest", false);

        String q2 = "2. How many years did \"The Hundred Years War\" last?";
        Map<String, Boolean> v2 = new HashMap<>();
        v1.put("116", true);
        v1.put("100", false);
        v1.put("99", false);
        v1.put("89", false);

        String q3 = "3. Did Julius Caesar conquer Gaul, imprisoning and ultimately executing their commander Vercingetorix in the process?";
        Map<String, Boolean> v3 = new HashMap<>();
        v1.put("true", true);
        v1.put("false", false);

        String q4 = "4. What was the life expectancy at birth, in 18th century Prussia?";
        Map<String, Boolean> v4 = new HashMap<>();
        v1.put("24.7", true);
        v1.put("37.2", false);
        v1.put("60.9", false);
        v1.put("41.5", false);

        questions.put(q1, v1);
        questions.put(q2, v2);
        questions.put(q3, v3);
        questions.put(q4, v4);
    }

    public String getQuestion(int questionNumber) {

        int i = 1;
        for (String q : questions.keySet()) {
            if (i == questionNumber) return q;
        }

        return "";
    }

    public List<String> getOptions(String question) {
        return new ArrayList<>(questions.get(question).keySet());
    }

    public boolean isCorrect(String answer, String question) {
        return questions.get(question).get(answer);
    }

    public int numberOfQuestions() {
        return questions.size();
    }
}
