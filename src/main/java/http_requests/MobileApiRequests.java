package http_requests;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class MobileApiRequests {

    /**
     * @param email of user
     * @param password of user
     * @return auth-hash
     */
    public static String Login(String email, String password) {

        HttpResponse<String> response = null;

        try {
            response = Unirest.post("https://api-stage5.vpn.fashiondays.com/mobile/18/customer/login")
                    .header("x-mobile-api-device-info", "ipad_8.4_1536x2048_2.0.0")
                    .header("x-mobile-app-locale", "ro_RO")
                    .header("x-mobile-app-version", "4.12")
                    .header("x-mobile-api-device-id", "irelevant-id")
                    .field("email", email)
                    .field("password", password)
                    .asString();
        } catch (UnirestException e) {
            System.out.println(e.getMessage());
        }

        return response == null ? "" : response.getHeaders().getFirst("x-mobile-api-new-auth-hash");
    }

    public static String CountryList() {
        HttpResponse<String> response = null;

        try {
            response = Unirest.get("https://api-stage5.vpn.fashiondays.com/mobile/18/country/list")
                    .header("x-mobile-api-device-info", "ipad_8.4_1536x2048_2.0.0")
                    .header("x-mobile-app-locale", "ro_RO")
                    .header("x-mobile-app-version", "4.12")
                    .header("x-mobile-api-device-id", "irelevant-id")
                    .asString();
        } catch (UnirestException e) {
            System.out.println(e.getMessage());
        }

        return response == null ? "" : response.getBody();
    }

    public static String CampaignsList() {
        HttpResponse<String> response = null;

        try {
            response = Unirest.get("https://api-stage5.vpn.fashiondays.com/mobile/18/campaign/list")
                    .header("x-mobile-api-device-info", "ipad_8.4_1536x2048_2.0.0")
                    .header("x-mobile-app-locale", "ro_RO")
                    .header("x-mobile-app-version", "4.12")
                    .header("x-mobile-api-device-id", "irelevant-id")
                    .asString();
        } catch (UnirestException e) {
            System.out.println(e.getMessage());
        }

        return response == null ? "" : response.getBody();
    }
}
