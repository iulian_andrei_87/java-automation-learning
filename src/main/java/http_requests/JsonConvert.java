package http_requests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonConvert {
    public static HashMap<String, String> toMap(String json) {
        Type type = new TypeToken<Map<String, String>>(){}.getType();

        return new Gson().fromJson(json, type);
    }

    public static ArrayList<String> toList(String json) {
        Type type = new TypeToken<List<String>>(){}.getType();

        return new Gson().fromJson(json, type);
    }
}
