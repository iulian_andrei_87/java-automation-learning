package objects_examples;

import java.util.ArrayList;

public class DtoExample {
    public static void main(String[] args) {
        //Populam DTO-ul
        ArrayList<String> sizesList = new ArrayList<>();
        sizesList.add("S");
        sizesList.add("M");
        sizesList.add("L");

        ProductDto product = new ProductDto();
        product.setProductId(11123);
        product.setName("Chilotei cu bulinute");
        product.setSizes(sizesList);

        //Printam valorile din DTO
        System.out.println(product.getProductId());
        System.out.println(product.getName());
        System.out.println(product.getSizes());
    }
}
