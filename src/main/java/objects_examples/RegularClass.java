package objects_examples;

public class RegularClass extends SuperClass {
    public String varName = "test";

    @Override
    public String getName() {
        return varName;
    }
}
