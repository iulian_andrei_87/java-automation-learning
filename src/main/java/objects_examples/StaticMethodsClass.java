package objects_examples;

public class StaticMethodsClass {
    public static int addNumbers(int n1, int n2) {
        return n1+n2;
    }

    public static String removeEmptySpaces(String str) {
        return str.replaceAll(" ", "");
    }
}
