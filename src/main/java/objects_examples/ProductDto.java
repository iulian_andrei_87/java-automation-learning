package objects_examples;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO (Data Transfer Object) / POJO (Plain Old Java Object)
 * Clasa ce tine doar informatii si este folosita pentru transferul de informatii
 *
 * Incapsulare
 * Cand campurile (variabile la nivel de clasa) sunt private iar accesul se face prin setter/getter
 */
public class ProductDto {

    private long productId;
    private String name;
    private ArrayList<String> sizes;

    /**
     * Setter
     *
     * @param productId
     */
    public void setProductId(long productId) {
        if (productId < 0) productId = 0;

        this.productId = productId;
    }

    /**
     * Getter
     *
     * @return
     */
    public long getProductId() {
        return productId;
    }

    /**
     * Getter
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Getter
     *
     * @return
     */
    public List<String> getSizes() {
        return sizes;
    }

    /**
     * Setter
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Setter
     *
     * @param sizes
     */
    public void setSizes(ArrayList<String> sizes) {
        this.sizes = sizes;
    }
}