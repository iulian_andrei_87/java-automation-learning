package objects_examples;

import java.util.HashMap;

public class ObjectsExample {

    public int testInt = 1;

    public static void main(String[] args) {

        // un obiect se intantiaza cu keywordul new
        // in cazul asta obiectul se numeste rexDog si este de tipul Dog
        Dog rexDog = new Dog("Rex", 10);

        System.out.println(Dog.instantiationCount);

        // in cazul asta obiectul se numeste fidoDog si este de tipul Dog
        Dog fidoDog = new Dog("Fido", 15);

        System.out.println(Dog.instantiationCount);

        new Dog();

        //example of how to use a static variable
        System.out.println(Dog.instantiationCount);

        Dog.toSpeak = "testx";


        // String este o clasa, iar fiecare String este de fapt un obiect de tip String
        String test = new String("test");

        System.out.println(test);
    }


    public void methodName() {

        String test = "tst";


        HashMap<String, String> mapName = new HashMap<>();
        mapName.put("cheie1", "valoare1");
        mapName.put("cheie2", "valoare2");
    }

    public void otherTest() {
//        String test2 = this.test;
        int test = this.testInt;
        this.methodName();
    }
}
