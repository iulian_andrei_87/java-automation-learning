package objects_examples;

public class StaticMethodsExample {
    public static void main(String[] args) {

        //Clasele ce folosesc numai metode statice nu se instantiaza. Se pot folosi direct.
        //(nu e nevoie de instantiere de obiecte)
        System.out.println(StaticMethodsClass.addNumbers(1, 10));
        System.out.println(StaticMethodsClass.removeEmptySpaces("test string that should have no more empty spaces"));
    }
}
