package objects_examples;

/**
 * Numele clase se scrie cu Camel Case cu prima litera mare NumeClasa
 */
public class Dog {

    //constanta se scrie cu litere mari si despartita prin undeline NUME_CONSTANTA
    public final static int MAX_AGE = 15;

    //variabile non-statice si se scrie cu Camel Case cu prima litera mica numeVariabila
    public int weight;
    public String name;

    //variabile statice si se scrie cu Camel Case cu prima litera mica numeVariabila
    static int instantiationCount = 0;

    /**
     * Constructor cu variabile
     *
     * @param name
     * @param weight
     */
    public Dog(String name, int weight) {
        this.name = name;
        this.weight = weight;

        instantiationCount += 1;
    }

    /**
     * Constructor fara variabile, constructor default
     *
     * In momentul cand clasa nu are un constructor, Java considera ca exista un constructor default
     */
    public Dog() {
        instantiationCount += 1;
    }

    public static String toSpeak;

    public void speak() {
        System.out.println(toSpeak);
    }

    public void setToSpeak(String toSpeak) {
        Dog.toSpeak = toSpeak;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
