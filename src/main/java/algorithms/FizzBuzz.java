package algorithms;

public class FizzBuzz {

    /**
     * Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of
     * the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and
     * five print “FizzBuzz”
     */
    public static void main(String[] args) {
        fizzBuzzExtended();
//        fizzBuzzShort();
    }

    private static void fizzBuzzExtended() {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    private static void fizzBuzzShort() {
        for (int i = 1; i <= 100; i++) {
            System.out.println((i % 3 == 0 || i % 5 == 0) ? ((i % 3 == 0) ? "Fizz" : "") + ((i % 5) == 0 ? "Buzz" : "") : String.valueOf(i));
        }
    }
}
