package algorithms;

import java.util.Arrays;

public class ArrayAlgo {

    public static void main(String[] args) {
        int[] array = {5, 5, 3, 7, 12};

        minNumber(array);
        System.out.println("===================================");
        System.out.println();
        maxNumber(array);
        System.out.println("===================================");
        System.out.println();
        minMaxSum(array);
        System.out.println("===================================");
        System.out.println();
    }

    /**
     * Calculate the minimum number from an array
     * [5,5,3,7,12]
     * <p>
     * 1. min = 5
     * 2. 5 > 5 => min nu se schimba
     * 3. 5 > 3 => min = 3
     * 4. 3 > 7 => min nu se schimba
     * 5. 3 > 12 => min nu se schimba
     */
    public static void minNumber(int[] listOfNumbers) {
        System.out.println("List is: " + Arrays.toString(listOfNumbers));

        int min = listOfNumbers[0];

        for (int i = 1; i < listOfNumbers.length; i++) {
            if (min > listOfNumbers[i]) min = listOfNumbers[i];
            System.out.println("Current min is: " + min);
            System.out.println();
        }

        System.out.println("Min from list is " + min);
    }

    /**
     * Calculate the maximum number from an array
     * [5,5,3,7,12]
     * <p>
     * 1. max = 5
     * 2. 5 < 5 => max nu se schimba
     * 3. 5 < 3 => max nu se schimba
     * 4. 5 < 7 => max = 7
     * 5. 7 < 12 => max = 12
     */
    public static void maxNumber(int[] listOfNumbers) {
        System.out.println("List is: " + Arrays.toString(listOfNumbers));

        int max = listOfNumbers[0];

        for (int i = 1; i < listOfNumbers.length; i++) {
            if (max < listOfNumbers[i]) max = listOfNumbers[i];
            System.out.println("Current max is: " + max);
            System.out.println();
        }

        System.out.println("Max from list is " + max);
    }

    /**
     * Calculate the sum between the min and max
     * [5,5,3,7,12]
     * <p>
     * 1. min = 5                       | max = 5
     * 2. 5 > 5 => min nu se schimba    | 5 < 5 => max nu se schimba
     * 3. 5 > 3 => min = 3              | 5 < 3 => max nu se schimba
     * 4. 3 > 7 => min nu se schimba    | 5 < 7 => max = 7
     * 5. 3 > 12 => min nu se schimba   | 7 < 12 => max = 12
     * 6. min (3) + max (12) = 15
     */
    public static void minMaxSum(int[] listOfNumbers) {
        System.out.println("List is: " + Arrays.toString(listOfNumbers));

        int min = listOfNumbers[0];
        int max = listOfNumbers[0];

        for (int i = 1; i < listOfNumbers.length; i++) {
            if (max < listOfNumbers[i]) max = listOfNumbers[i];
            else if (min > listOfNumbers[i]) min = listOfNumbers[i];

            System.out.println("Current min is: " + min);
            System.out.println("Current max is: " + max);
            System.out.println();
        }

        System.out.println("Sum of min (" + min + ") and max (" + max + ") is " + (min + max));
    }
}