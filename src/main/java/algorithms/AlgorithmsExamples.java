package algorithms;

import java.util.Arrays;

public class AlgorithmsExamples {

    /**
     * sorting array and list permutations
     * [6,3,8,2,3] => [2,3,3,6,8]
     * [2,1,6,4] => [1,2,4,6]
     */
    public static void sortArray(int[] listOfNumbers) {

        int temp;

        for (int j = 0; j < listOfNumbers.length - 1; j++) {
            System.out.println(Arrays.toString(listOfNumbers));

            for (int i = listOfNumbers.length - 2; i > 0; i--) {

                if (listOfNumbers[i] > listOfNumbers[i + 1]) {
                    temp = listOfNumbers[i];
                    listOfNumbers[i] = listOfNumbers[i + 1];
                    listOfNumbers[i + 1] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(listOfNumbers));
    }

    // palindrom, suma cifrelor + cifra finala, fibonacci, factorial, cautarea binara
}
