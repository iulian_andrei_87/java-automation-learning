package algorithms;

import java.util.Arrays;

public class PalindromeAlgo {
    public static void main(String[] args) {
        int palindromeNumber = 12321;
        int secondPalindromeNumber = 123321;
        int nonPalindromeNumber = 123456;

        isNumericPalindrome(palindromeNumber);
        System.out.println();
        isNumericPalindrome(secondPalindromeNumber);
        System.out.println();
        isNumericPalindrome(nonPalindromeNumber);
        System.out.println();

        transformNumberToPalindrome(123);
        transformNumberToPalindrome(1233); //TODO fix this
    }

    /**
     * Find if a number is a palindrome
     * number 12321
     * <p>
     * 1. length = 1 | numbersList[1]
     * 2. length = 2 | numbersList[1, 2]
     * 3. length = 3 | numbersList[1, 2, 3]
     * 4. length = 4 | numbersList[1, 2, 3, 2]
     * 5. length = 5 | numbersList[1, 2, 3, 2, 1]
     * 6. if (1 != 1 && 0 < 2(half)) number isn't palindrome (is palindrome)
     * 7. if (2 != 2 && 1 < 2) number isn't palindrome (is palindrome)
     * 8. number is palindrome
     * <p>
     * number 1221
     * <p>
     * 1. length = 1 | numbersList[1]
     * 2. length = 2 | numbersList[1, 2]
     * 4. length = 3 | numbersList[1, 2, 2]
     * 5. length = 4 | numbersList[1, 2, 2, 1]
     * 6. if (1 != 1 && 0 < 2(half)) number isn't palindrome (is palindrome)
     * 7. if (2 != 2 && 1 < 2(half)) number isn't palindrome (is palindrome)
     * 8. number is palindrome
     * <p>
     * number 1234
     * <p>
     * 1. length = 1 | numbersList[1]
     * 2. length = 2 | numbersList[1, 2]
     * 3. length = 3 | numbersList[1, 2, 3]
     * 4. length = 4 | numbersList[1, 2, 3, 4]
     * 6. if (1 != 4 && 0 < 2) number isn't palindrome
     * 8. number isn't palindrome
     */
    public static void isNumericPalindrome(int number) {

        int[] numbersList = new int[100];

        int length = 0;
        while (number != 0) {
            numbersList[length] = number % 10;
            number /= 10;

            length++;
        }

        numbersList = Arrays.copyOf(numbersList, length);

        for (int i = 0; i < length - 1; i++) {
            if (numbersList[i] != numbersList[(length - i) - 1] && i < length / 2) {
                System.out.println("Number isn't a Palindrome");
                return;
            }
        }

        System.out.println("Number is a Palindrome");
    }

    /**
     * Take number and transform to palindrome
     * num = 123
     * temp = 123;
     * <p>
     * 1. number = (123 * 10) + 123 % 10 = 1233
     * 2. number = (1233 * 10) + 1233 % 10 = 12332
     * 3. number = (12332 * 10) + 12332 % 10 = 123321
     * 4. number = 123321
     */
    public static void transformNumberToPalindrome(int number) {
        System.out.println("Initial number: " + number);
        int temp = number;

        while (temp != 0) {
            number = (number * 10) + temp % 10;
            temp /= 10;
        }

        System.out.println("Palindrome number: " + number);
    }
}