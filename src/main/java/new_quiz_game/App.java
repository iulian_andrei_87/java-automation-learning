package new_quiz_game;

import new_quiz_game.controllers.QuestionController;
import new_quiz_game.db.InjectQuestions;

public class App {
    public static void main(String[] args) {

        InjectQuestions injector = new InjectQuestions();
        injector.inject();

        new QuestionController()
                .run(injector.getQuestions());
    }
}