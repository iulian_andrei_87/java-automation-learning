package new_quiz_game.models;

public class ScoringModel {
    private int correctAnswers;
    private int answeredQuestions;

    public void updateCorrectAnswers() {
        this.correctAnswers++;
    }

    public void updateTotalAnsweredQuestions() {
        this.answeredQuestions++;
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public int getAnsweredQuestions() {
        return answeredQuestions;
    }
}
