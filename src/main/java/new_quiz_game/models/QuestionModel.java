package new_quiz_game.models;

import new_quiz_game.db.Question;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionModel {

    private String givenAnswer;
    private Question question;

    private ScoringModel scoring = new ScoringModel();

    private static Map<String, Integer> variantsAnswerMapping = new HashMap<>();

    static {
        variantsAnswerMapping.put("a", 0);
        variantsAnswerMapping.put("b", 1);
        variantsAnswerMapping.put("c", 2);
        variantsAnswerMapping.put("d", 3);
    }

    public void validateAnswer() {
        String variant = this.getVariantValue(this.givenAnswer, this.question.getAnswerVariants());

        if (variant.equals(this.question.getCorrectAnswer())) {
            this.scoring.updateCorrectAnswers();
            System.out.println("Ai raspuns corect");
        } else {
            System.out.println("Ai raspuns gresit");
        }

        this.scoring.updateTotalAnsweredQuestions();
    }

    private String getVariantValue(String fromKeyboard, List<String> variants) {

        if (!variantsAnswerMapping.containsKey(fromKeyboard))
            return "";

        return variants.get(variantsAnswerMapping.get(fromKeyboard));
    }

    public int getCorrectAnsweredQuestion() {
        return this.scoring.getCorrectAnswers();
    }

    public int getTotalAnsweredQuestions() {
        return this.scoring.getAnsweredQuestions();
    }

    public void setGivenAnswer(String givenAnswer) {
        this.givenAnswer = givenAnswer;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
