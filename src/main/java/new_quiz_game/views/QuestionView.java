package new_quiz_game.views;

import new_quiz_game.db.Question;

import java.util.List;

public class QuestionView {

    private Question question;

    public QuestionView(Question question) {
        this.question = question;
    }

    public void printAll() {
        this.printQuestion();
        this.printAnswerVariants();
    }

    private void printLine() {
        System.out.println("===============================================");
    }

    private void printQuestion() {
        this.printLine();

        System.out.println(this.question.getQuestion());
        System.out.println();
    }

    private void printAnswerVariants() {

        List<String> listOfVariants = this.question.getRandomVariants();

        int ascii = 97;

        for (int i = 0; i < listOfVariants.size(); i++) {
            System.out.println((char) (ascii + i) + ". " + listOfVariants.get(i));
        }

        this.printLine();
    }
}
