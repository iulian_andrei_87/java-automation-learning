package new_quiz_game.views;

public class ScoringView {

    private int score;
    private int totalQuestions;

    public ScoringView(int score, int totalQuestions) {
        this.score = score;
        this.totalQuestions = totalQuestions;
    }

    public void showScore() {
        System.out.println("Ai raspuns la corect la " + this.score + " din " + this.totalQuestions);
    }
}
