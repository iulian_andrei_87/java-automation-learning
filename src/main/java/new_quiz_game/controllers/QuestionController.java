package new_quiz_game.controllers;

import new_quiz_game.db.Question;
import new_quiz_game.models.QuestionModel;
import new_quiz_game.utill.UserInput;
import new_quiz_game.views.QuestionView;
import new_quiz_game.views.ScoringView;

import java.util.List;

public class QuestionController {

    public void run(List<Question> questionList) {
        QuestionModel qm = new QuestionModel();

        for (Question question : questionList) {
            QuestionView questionView = new QuestionView(question);
            questionView.printAll();

            qm.setQuestion(question);
            qm.setGivenAnswer(UserInput.grabString());

            qm.validateAnswer();
        }

        ScoringView scoringView = new ScoringView(
                qm.getCorrectAnsweredQuestion(),
                qm.getTotalAnsweredQuestions()
        );

        scoringView.showScore();
    }
}
