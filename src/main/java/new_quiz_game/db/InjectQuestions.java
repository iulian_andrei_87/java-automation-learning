package new_quiz_game.db;

import java.util.ArrayList;
import java.util.List;

public class InjectQuestions {

    private List<Question> questions = new ArrayList<>();

    public void inject() {
        ArrayList<String> answerVariants1 = new ArrayList<>();
        answerVariants1.add("The Battle of Lepanto");
        answerVariants1.add("The Battle of Athens");
        answerVariants1.add("The Battle of Berlin");
        answerVariants1.add("The Battle of Bucharest");

        Question question1 = new Question();
        question1.setQuestion("Which battle of 1571 marked the end of the Ottoman naval supremacy in the Mediterranean?");
        question1.setCorrectAnswer("The Battle of Lepanto");
        question1.setAnswerVariants(answerVariants1);

        ArrayList<String> answerVariants2 = new ArrayList<>();
        answerVariants2.add("116");
        answerVariants2.add("100");
        answerVariants2.add("99");
        answerVariants2.add("89");

        Question question2 = new Question();
        question2.setQuestion("How many years did \"The Hundred Years War\" last?");
        question2.setCorrectAnswer("116");
        question2.setAnswerVariants(answerVariants2);

        ArrayList<String> answerVariants3 = new ArrayList<>();
        answerVariants3.add("true");
        answerVariants3.add("false");

        Question question3 = new Question();
        question3.setQuestion("Did Julius Caesar conquer Gaul, imprisoning and ultimately executing their commander Vercingetorix in the process?");
        question3.setCorrectAnswer("true");
        question3.setAnswerVariants(answerVariants3);

        ArrayList<String> answerVariants4 = new ArrayList<>();
        answerVariants4.add("24.7");
        answerVariants4.add("37.2");
        answerVariants4.add("60.9");
        answerVariants4.add("41.5");

        Question question4 = new Question();
        question4.setQuestion("What was the life expectancy at birth, in 18th century Prussia?");
        question4.setCorrectAnswer("24.7");
        question4.setAnswerVariants(answerVariants4);

        this.questions.add(question1);
        this.questions.add(question2);
        this.questions.add(question3);
        this.questions.add(question4);
    }

    public List<Question> getQuestions() {
        return this.questions;
    }
}
