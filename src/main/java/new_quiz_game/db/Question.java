package new_quiz_game.db;

import java.util.Collections;
import java.util.List;

public class Question {

    private String question;
    private String correctAnswer;
    private List<String> answerVariants;

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public List<String> getAnswerVariants() {
        return this.answerVariants;
    }

    public List<String> getRandomVariants() {
        Collections.shuffle(this.answerVariants);

        return this.answerVariants;
    }

    public void setAnswerVariants(List<String> answerVariants) {

        if (answerVariants.size() < 2) {
            throw new RuntimeException("Not enough answer variants were given! Minimum number of variants is 2.");

        } else if (answerVariants.size() > 4) { //TODO check if answers contains correct one etc
            this.answerVariants.addAll(answerVariants.subList(0, 4));

        } else if (answerVariants.size() == 3) { //TODO check if answers contains correct one etc
            this.answerVariants.addAll(answerVariants.subList(0, 2));

        } else {
            this.answerVariants = answerVariants;
        }
    }
}
