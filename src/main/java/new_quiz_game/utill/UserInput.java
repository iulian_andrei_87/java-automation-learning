package new_quiz_game.utill;

import java.util.Scanner;

public class UserInput {

    public static String grabString() {
        Scanner scan = new Scanner(System.in);

        return scan.next();
    }
}
